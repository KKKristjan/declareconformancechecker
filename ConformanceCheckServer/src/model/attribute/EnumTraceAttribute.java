package model.attribute;

import java.util.List;

public class EnumTraceAttribute {
    String name;
    List<String> params;

    public EnumTraceAttribute(String name, List<String> params) {
        this.name = name;
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public List<String> getParams() {
        return params;
    }
}

