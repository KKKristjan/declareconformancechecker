package model;

import model.attribute.EnumTraceAttribute;
import model.attribute.FloatTraceAttribute;
import model.attribute.IntTraceAttribute;
import model.data.EnumeratedData;
import model.data.FloatData;
import model.data.IntegerData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeclareModel {
    List<Activity> activities;
    List<Constraint> constraints;
    List<DataConstraint> dataConstraints;

    List<EnumeratedData> enumeratedData;
    List<IntegerData> integerData;
    List<FloatData> floatData;

    Map<String, List<String>> activityToData;
    Map<String, List<String>> dataToActivity;

    List<EnumTraceAttribute> enumTraceAttributes;
    List<IntTraceAttribute> intTraceAttributes;
    List<FloatTraceAttribute> floatTraceAttributes;


    public DeclareModel() {
        activities = new ArrayList<>();
        enumeratedData = new ArrayList<>();
        constraints = new ArrayList<>();
        dataConstraints = new ArrayList<>();

        activityToData = new HashMap<>();
        dataToActivity = new HashMap<>();

        integerData = new ArrayList<>();
        floatData = new ArrayList<>();
        enumeratedData = new ArrayList<>();

        intTraceAttributes = new ArrayList<>();
        floatTraceAttributes = new ArrayList<>();
        enumTraceAttributes = new ArrayList<>();

    }

    public List<Activity> getActivities() {
        return activities;
    }

    public List<EnumeratedData> getEnumeratedData() {
        return enumeratedData;
    }

    public void setEnumeratedData(List<EnumeratedData> enumeratedData) {
        this.enumeratedData = enumeratedData;
    }

    public List<IntegerData> getIntegerData() {
        return integerData;
    }

    public void setIntegerData(List<IntegerData> integerData) {
        this.integerData = integerData;
    }

    public List<FloatData> getFloatData() {
        return floatData;
    }

    public void setFloatData(List<FloatData> floatData) {
        this.floatData = floatData;
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<Constraint> constraints) {
        this.constraints = constraints;
    }

    public List<DataConstraint> getDataConstraints() {
        return dataConstraints;
    }

    public void setDataConstraints(List<DataConstraint> dataConstraints) {
        this.dataConstraints = dataConstraints;
    }

    public Map<String, List<String>> getActivityToData() {
        return activityToData;
    }

    public void setActivityToData(Map<String, List<String>> activityToData) {
        this.activityToData = activityToData;
    }

    public Map<String, List<String>> getDataToActivity() {
        return dataToActivity;
    }

    public void setDataToActivity(Map<String, List<String>> dataToActivity) {
        this.dataToActivity = dataToActivity;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public List<EnumTraceAttribute> getEnumTraceAttributes() {
        return enumTraceAttributes;
    }

    public void setEnumTraceAttributes(List<EnumTraceAttribute> enumTraceAttributes) {
        this.enumTraceAttributes = enumTraceAttributes;
    }

    public List<IntTraceAttribute> getIntTraceAttributes() {
        return intTraceAttributes;
    }

    public void setIntTraceAttributes(List<IntTraceAttribute> intTraceAttributes) {
        this.intTraceAttributes = intTraceAttributes;
    }

    public List<FloatTraceAttribute> getFloatTraceAttributes() {
        return floatTraceAttributes;
    }

    public void setFloatTraceAttributes(List<FloatTraceAttribute> floatTraceAttributes) {
        this.floatTraceAttributes = floatTraceAttributes;
    }

    @Override
    public String toString() {
        return "DeclareModel{" +
                "activities=" + activities +
                ", constraints=" + constraints +
                ", dataConstraints=" + dataConstraints +
                '}';
    }
}