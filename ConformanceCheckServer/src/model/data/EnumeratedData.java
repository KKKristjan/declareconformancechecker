package model.data;

import java.util.ArrayList;
import java.util.List;

public class EnumeratedData {
    String type;
    List<String> values;

    public EnumeratedData() {
        values = new ArrayList<>();
    }

    public EnumeratedData(String type, List<String> values) {
        this.type = type;
        this.values = new ArrayList<>(values);
    }

    public String getType() {
        return type;
    }

    public List<String> getValues() {
        return values;
    }
}

