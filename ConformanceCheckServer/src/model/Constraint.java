package model;

import java.util.List;

public class Constraint {
    String name;
    List<String> args;
    Statement statement;
    State state;

    public Constraint(String name, List<String> args, Statement statement) {
        this.name = name;
        this.args = args;
        this.statement = statement;
    }

    public String getName() {
        return name;
    }

    public List<String> getArgs() {
        return args;
    }

    @Override
    public String toString() {
        return "Constraint{" +
                "name='" + name + '\'' +
                ", args=" + args +
                ", statement=" + statement +
                '}';
    }

    public String taskA() {
        return args.get(0);
    }

    public String taskB() {
        return args.get(1);
    }

    public Statement getStatement() {
        return statement;
    }

    public boolean isBinary(){
        return args.size() == 2;
    }

    public enum State{
        POSSIBLY_SATISFIED, POSSIBLY_VIOLATED, PERMANENTLY_VIOLATED, PERMANENTLY_SATISFIED, STATE_CONFLICT
    }

    public boolean supportsVacuity() {
        return isBinary() && (getName().equals("RespondedExistence") ||
                getName().equals("Response") ||
                getName().equals("AlternateResponse") ||
                getName().equals("ChainResponse") ||
                getName().equals("Precedence") ||
                getName().equals("AlternatePrecedence") ||
                getName().equals("ChainPrecedence") ||
                getName().equals("NotRespondedExistence") ||
                getName().equals("NotResponse") ||
                getName().equals("NotPrecedence") ||
                getName().equals("NotChainResponse") ||
                getName().equals("NotChainPrecedence"));
    }
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}

