package model;

public class Statement {
    String code;
    int line;
    public Statement(String code, int line){
        this.code = code;
        this.line = line;
    }

    public String getCode() {
        return code;
    }

    public int getLine() {
        return line;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "code='" + code + '\'' +
                ", line=" + line +
                '}';
    }
}