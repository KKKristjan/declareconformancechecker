package model;

import java.util.List;

public class Trace {
    List<Activity> activities;

    public Trace(List<Activity> activities) {
        this.activities = activities;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
