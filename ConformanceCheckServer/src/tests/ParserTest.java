package tests;

import exceptions.DeclareParserException;
import model.DataConstraint;
import model.Statement;
import org.junit.jupiter.api.Test;
import parser.DeclareParser;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTest {
    DeclareParser parser = new DeclareParser();

    @Test
    public void testDataConstraints() throws DeclareParserException {
        List<Statement> raw = Stream.of("Absence[BookTransport A] | A.Price is High and A.Speed is Low",
                "RespondedExistence[BookTransport A, UseTransport B] | A.TransportType is Car | B.Speed is not Low")
                .map(i -> new Statement(i, 0)).collect(Collectors.toList());
        List<DataConstraint> dcs = parser.parseDataConstraints(raw);
        assertEquals(dcs.size(), 2);
        DataConstraint first = dcs.get(0);
        DataConstraint second = dcs.get(1);
        assertEquals(first.getName(), "Absence");
        assertEquals(second.getName(), "RespondedExistence");
        assertEquals(first.taskA(), "BookTransport");
        assertEquals(second.taskA(), "BookTransport");
        assertEquals(second.taskB(), "UseTransport");
        assertEquals(first.getFirstFunction().getExpression().getNode().getValue(), "and");
        assertEquals(second.getSecondFunction().getExpression().getNode().getValue(), "is not");
    }
}
