public class CLI {

    public static Configuration getConfigFromArgs(String[] args) {
        Configuration config = new Configuration();
        if (args.length == 1){
            for(int i = 0; i < args.length; ++i){
                if (args[i].equals("-port")){
                    config.port = Integer.parseInt(getArg(args, ++i, "port"));
                }
                else {
                    throw new IllegalArgumentException("Unknown argument '" + args[i] + "'");
                }
            }
        }
        else{
            config.port = 9876;
        }
        return config;
    }
    private static String getArg(String[] args, int i, String name) {
        if (args.length <= i)
            throw new IndexOutOfBoundsException("Value for " + name + "required but not found");
        return args[i];
    }
}
