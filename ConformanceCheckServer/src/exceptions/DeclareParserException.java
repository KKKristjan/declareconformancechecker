package exceptions;

public class DeclareParserException extends Exception {
    public DeclareParserException(String message) {
        super(message);
    }
}
