package exceptions;

public class BadSolutionException extends Exception {
    public BadSolutionException(String message) {
        super(message);
    }
}
