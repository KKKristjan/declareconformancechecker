package helpers;

import java.util.UUID;

public class RandomHelper {
    static int start = 99999;

    public static int getNext() {
        return ++start;
    }

    public static String getName() {
        return "x" + UUID.randomUUID().toString().replace('-','x');
    }
}
