package alloycode.model.data;

import java.util.ArrayList;
import java.util.List;

public class EnumeratedDataImpl {
    String type;
    List<String> values;

    public EnumeratedDataImpl() {
        values = new ArrayList<>();
    }

    public EnumeratedDataImpl(String type, List<String> values) {
        this.type = type;
        this.values = new ArrayList<>(values);
    }

    public String getType() {
        return type;
    }

    public List<String> getValues() {
        return values;
    }

    public void addValue(String value) {
        this.values.add(value);
    }
}