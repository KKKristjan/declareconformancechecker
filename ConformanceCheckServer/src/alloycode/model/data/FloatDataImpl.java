package alloycode.model.data;

import alloycode.interfaces.SafeFunction2;
import alloycode.model.intervals.FloatInterval;
import alloycode.model.intervals.FloatValue;
import alloycode.model.intervals.IntervalSplit;
import exceptions.GenerationException;
import helpers.RandomHelper;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public class FloatDataImpl extends NumericDataImpl {
    float min;
    float max;
    boolean includeMin;
    boolean includeMax;
    int intervalSplits;
    SafeFunction2 valueGenerator;

    public FloatDataImpl(String type, float min, float max, int intervalSplits, SafeFunction2<Float, Float, Float> valueGenerator) {
        this.min = min;
        this.max = max;
        this.type = type;
        this.intervalSplits = intervalSplits;
        this.valueGenerator = valueGenerator;
    }

    @Override
    protected void generate() {
        intervals = new HashMap<>();

        if (splits.size() == 0) {
            addBetweenInterval(Pair.of(min, false), Pair.of(max, true));
            return;
        }

        List<Pair<Float, Boolean>> floatValues = splits.stream().map(i -> Pair.of(i.getParsedValue(Float::parseFloat), i.isRight())).distinct().collect(Collectors.toList());

        if (floatValues.get(0).getKey() > min)
            floatValues.add(0, Pair.of(min, false));
        if (floatValues.get(floatValues.size() - 1).getKey() < max)
            floatValues.add(floatValues.size(), Pair.of(max, true));

        if (includeMin)
            intervals.put(formatEquals(floatValues.get(0).getKey()), new FloatValue(floatValues.get(0).getKey()));

        if (includeMax)
            intervals.put(formatEquals(floatValues.get(floatValues.size() - 1).getKey()), new FloatValue(floatValues.get(floatValues.size() - 1).getKey()));

        addValues(splits);
        addIntervals(floatValues);
    }

    private void addValues(List<IntervalSplit> splits) {
        for (IntervalSplit i : splits.stream().filter(i -> i.isLeft() && i.isRight()).collect(Collectors.toList())) {
            intervals.put(formatEquals(i.getParsedValue(Float::parseFloat)), new FloatValue(i.getParsedValue(Float::parseFloat)));
        }

        java.util.Map<Float, Boolean> a = new HashMap<>();
        for (IntervalSplit i : splits.stream().filter(i -> i.isLeft() ^ i.isRight()).collect(Collectors.toList())) {
            Float value = i.getParsedValue(Float::parseFloat);
            if (a.containsKey(value) && a.get(value) == i.isRight())
                intervals.put(formatEquals(value), new FloatValue(value));
            else
                a.put(value, i.isLeft());
        }
    }

    private void addIntervals(List<Pair<Float, Boolean>> floatValues) {
        for (int i = 1; i < floatValues.size(); ++i) {
            addBetweenInterval(floatValues.get(i - 1), floatValues.get(i));
        }
    }


    private void addBetweenInterval(Pair<Float, Boolean> left, Pair<Float, Boolean> right) {
        float a = left.getKey();
        float b = right.getKey();
        float step = (b - a) / intervalSplits;
        for (int j = 0; j < intervalSplits; ++j) {
            float start = a + step * j;
            float end = a + step * (j + 1);
            intervals.put(formatBetween(start, end), new FloatInterval(start, end, !left.getRight(), right.getRight(), valueGenerator));
        }
    }

    @Override
    public void addSplit(IntervalSplit s) throws GenerationException {
        float val = s.getParsedValue(Float::parseFloat);
        if (val < min || val > max)
            throw new GenerationException(val + " is out of defined float interval " + min + "" + max);
        if (val == min)
            includeMin = true;
        if (val == max)
            includeMax = true;
        this.splits.add(s);
    }

    private String formatBetween(float a, float b) {
        return ("floatBetween" + String.valueOf(a).replace('.', 'p') + "and" + String.valueOf(b).replace('.', 'p') + 'r' + RandomHelper.getNext()).replace('-', 'm');
    }

    private String formatEquals(float a) {
        return ("floatEqualsTo" + String.valueOf(a).replace('.', 'p') + 'r' + RandomHelper.getNext()).replace('-', 'm');
    }
}