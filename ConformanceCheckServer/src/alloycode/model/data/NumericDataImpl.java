package alloycode.model.data;

import alloycode.model.intervals.Interval;
import alloycode.model.intervals.IntervalSplit;
import exceptions.DeclareParserException;
import exceptions.GenerationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class NumericDataImpl extends EnumeratedDataImpl {
    Map<String, Interval> intervals;
    List<IntervalSplit> splits = new ArrayList<>();

    @Override
    public List<String> getValues() {
        if (intervals == null)
            generate();
        return new ArrayList<>(intervals.keySet());
    }

    public Map<String, Interval> getMapping() {
        if (intervals == null)
            generate();
        return intervals;
    }

    @Override
    public final void addValue(String value) {
        throw new UnsupportedOperationException("Use 'addSplit' method for numeric data");
    }

    public abstract void addSplit(IntervalSplit s) throws GenerationException, DeclareParserException;

    protected abstract void generate();

}
