package alloycode.interfaces;

public interface SafeFunction1<T, R>  {
    R invoke(T t);
}