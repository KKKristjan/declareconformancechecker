package runner;

import alloycode.ConstraintChecker;
import edu.mit.csail.sdg.alloy4.Err;
import exceptions.DeclareParserException;
import exceptions.GenerationException;
import model.DeclareModel;
import model.Statement;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import parser.DeclareParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MonitorRunner {

    //String model;
    String trace;
    XTraceImpl oneTrace = new XTraceImpl(new XAttributeMapImpl());
    DeclareModel model = new DeclareModel();
    DeclareParser modelParser = new DeclareParser();
    boolean conflict;
    OSXMLConverter osxmlConverter = new OSXMLConverter();
    ConstraintChecker constraintChecker = new ConstraintChecker();
    int overallNumberOfConstraints = 0;
    public Map<String, Long> times = new HashMap<>();
    int r = 2;

    public MonitorRunner(boolean conflict) {
        this.conflict = conflict;
    }


    public void setModel(String stringModel) throws DeclareParserException {
        String modelWithDummyStart = "activity complete \n" + stringModel;
        modelParser.Parse(modelWithDummyStart, model, true);
        //We setModel in the constraintChecker
        constraintChecker.setModel(model);
        constraintChecker.setConstraints(model.getConstraints());
        constraintChecker.setDataConstraints(model.getDataConstraints());
        constraintChecker.setAllConstraintArr();
        List<Statement> dataConstraintsCode = modelParser.getDataConstraintsCode();
        int i = 0;
        String[] dataConstraintNameCode = new String[model.getDataConstraints().size()];
        for (Statement st : dataConstraintsCode) {
            System.out.println("st" + st.getCode());
            dataConstraintNameCode[i] = st.getCode();
            i++;
        }
        constraintChecker.setDataConstraintsName(dataConstraintNameCode);
        constraintChecker.setConstraintStringNames();
        overallNumberOfConstraints = model.getDataConstraints().size() + model.getConstraints().size();
    }

    public String setTrace(String stringTrace) throws DeclareParserException, GenerationException, Err {
        String answer;
        XTrace t;
        t = (XTrace) osxmlConverter.fromXML(stringTrace);
        XEvent e = t.get(0);
        try {
            oneTrace.add(e);
            constraintChecker.setTrace(oneTrace);
            if (oneTrace.size() == 1) {
                constraintChecker.initMatrix();
            }
            if (e.getAttributes().get("concept:name").toString().equals("complete")) {
                oneTrace.add(e);
                constraintChecker.setTrace(oneTrace);
                if (conflict) {
                    constraintChecker.run();
                }
                constraintChecker.setFinal();
                answer = constraintChecker.updatedString();
                oneTrace = new XTraceImpl(new XAttributeMapImpl());
                // Restart things
                constraintChecker.setConflictedConstraints(new ArrayList<>());
                //constraintChecker.setPermViolatedCon(new ArrayList<>());
                for (String name : times.keySet()) {
                    String key = name.toString();
                    String value = times.get(name).toString();
                    System.out.println(key + " " + value);
                }
            } else {
                //Instant start = Instant.now();
                constraintChecker.run();
                //Instant finish = Instant.now();
                //long timeElapsed = Duration.between(start, finish).toMillis();
                //times.put((e.getAttributes().get("concept:name").toString() + iterator), timeElapsed);
                //iterator++;
                if (conflict) {
                    boolean subListCheck = constraintChecker.checkFullConjuction();// here we find out whether we need to do any further investigation. kui see on false
                    boolean inWhile = !subListCheck;// siis see on true, ja kui eelnev on true, siis see on false ja see on ka see, mida me tahame, sest eelmine konjuktsioon on ok.
                    while (inWhile && ((r <= (overallNumberOfConstraints)) || overallNumberOfConstraints == 2)) {// mis tähendab, et siinolemine on ull timm
                        inWhile = constraintChecker.checkSublistConjunction(overallNumberOfConstraints, r);//
                        if (overallNumberOfConstraints == 2) {
                            break;
                        } else {
                            r++;
                        }
                    }
                    r = 2;// should be two again, so that we would check the value again afterwards
                }
                answer = constraintChecker.updatedString();
            }
            return answer;
        }catch (Throwable err) {
            err.printStackTrace();
            return "nothing";
        }
    }
}
