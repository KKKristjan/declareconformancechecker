import alloycode.ConstraintChecker;
import alloycode.NameEncoder;
import global.Global;
import model.DeclareModel;
import model.Statement;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.*;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import parser.DeclareParser;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class Main {
    private static ServerSocket server;
    private static int port = 9875;
    static OSXMLConverter osxmlConverter = new OSXMLConverter();
    static boolean conflictCheck = true;
    static int r = 2;
    public static Map<String, Long> times = new HashMap<>();

    public static void main(String[] args) throws Exception {
        XTraceImpl oneTrace = new XTraceImpl(new XAttributeMapImpl());
        server = new ServerSocket(port);
        for(String s : args){
            System.out.println("args " + s);
            if (s.equals("-conflictCheck")){
                conflictCheck = true;
            }
        }

        String sentMessage;
        String sentTrace;
        String sentModel;
        DeclareModel model = new DeclareModel();
        DeclareParser modelParser = new DeclareParser();
        //NameEncoder encoder = new NameEncoder(modelParser);
        ConstraintChecker constraintChecker = new ConstraintChecker();
        int overallNumberOfConstraints = 0;
        while (true) {
            System.out.println("\nWaiting for client request");
            Socket socket = server.accept();
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            sentMessage = (String) ois.readObject();
            System.out.println("Received Message: " + sentMessage);
            if (sentMessage.equalsIgnoreCase("model")) {
                sentModel = receive(socket);
                System.out.println("Model Received: \n" + sentModel);
                String modelWithDummyStart = "activity complete \n" + sentModel;
                //if (Global.encodeNames)
                  //  modelWithDummyStart = encoder.encode(modelWithDummyStart);
                //Instant start = Instant.now();
                modelParser.Parse(modelWithDummyStart, model, true);
                //Instant finish = Instant.now();
                //long timeElapsed = Duration.between(start, finish).toMillis();
                //times.put("model", timeElapsed);


                //We setModel in the constraintChecker
                constraintChecker.setModel(model);
                constraintChecker.setConstraints(model.getConstraints());
                constraintChecker.setDataConstraints(model.getDataConstraints());
                constraintChecker.setAllConstraintArr();
                List<Statement> dataConstraintsCode = modelParser.getDataConstraintsCode();
                int i = 0;
                String[] dataConstraintNameCode = new String[model.getDataConstraints().size()];
                for (Statement st : dataConstraintsCode) {
                    System.out.println("st" + st.getCode());
                    dataConstraintNameCode[i] = st.getCode();
                    i++;
                }
                constraintChecker.setDataConstraintsName(dataConstraintNameCode);
                constraintChecker.setConstraintStringNames();
                overallNumberOfConstraints = model.getDataConstraints().size() + model.getConstraints().size();
                //System.out.println(constraintChecker.checkModel(2, 7));
                //Send to socket
                //send(socket, "Received");
            }
            if (sentMessage.equalsIgnoreCase("trace")) {
                sentTrace = receive(socket);
                System.out.println("Received event: " + sentTrace);
                XTrace t;
                t = (XTrace) osxmlConverter.fromXML(sentTrace);
                XEvent e = t.get(0);
                oneTrace.add(e);
                System.out.println(e.getAttributes());
                constraintChecker.setTrace(oneTrace);
                if (oneTrace.size() == 1) {
                    constraintChecker.initMatrix();
                }
                if (e.getAttributes().get("concept:name").toString().equals("complete")) {
                    oneTrace.add(e);
                    constraintChecker.setTrace(oneTrace);
                    if (conflictCheck) {
                        constraintChecker.run();
                    }
                    constraintChecker.setFinal();
                    String answer = constraintChecker.updatedString();
                    send(socket, answer);
                    oneTrace = new XTraceImpl(new XAttributeMapImpl());
                    // Restart things
                    constraintChecker.setConflictedConstraints(new ArrayList<>());
                    //constraintChecker.setPermViolatedCon(new ArrayList<>());
                    for (String name: times.keySet()){
                        String key = name.toString();
                        String value = times.get(name).toString();
                        System.out.println(key + " " + value);
                    }
                } else {
                    //Instant start = Instant.now();
                    constraintChecker.run();
                    //Instant finish = Instant.now();
                    //long timeElapsed = Duration.between(start, finish).toMillis();
                    //times.put((e.getAttributes().get("concept:name").toString() + iterator), timeElapsed);
                    //iterator++;
                    if (conflictCheck) {
                        boolean subListCheck = constraintChecker.checkFullConjuction();// here we find out whether we need to do any further investigation. kui see on false
                        boolean inWhile = !subListCheck;// siis see on true, ja kui eelnev on true, siis see on false ja see on ka see, mida me tahame, sest eelmine konjuktsioon on ok.
                        while (inWhile && ((r <= (overallNumberOfConstraints)) || overallNumberOfConstraints == 2)) {// mis tähendab, et siinolemine on ull timm
                            inWhile = constraintChecker.checkSublistConjunction(overallNumberOfConstraints, r);//
                            if (overallNumberOfConstraints == 2){
                                break;
                            }else {
                                r++;
                            }
                        }
                        r = 2;// should be two again, so that we would check the value again afterwards
                    }
                    String answer = constraintChecker.updatedString();
                    //System.out.println("answer: " + answer);
                    send(socket, answer);
                }
            }
            if (sentMessage.equalsIgnoreCase("trace end")) {
                constraintChecker.setFinal();
                oneTrace = new XTraceImpl(new XAttributeMapImpl());
            }
            if (sentMessage.equalsIgnoreCase("exit")) {// should do this path aswell
                break;
            }
        }
        server.close();
        System.out.println("Shutting down now");
        System.exit(0);
    }
    //Get Trace and/or model from client
    private static String receive(Socket socket) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        return (String) ois.readObject();
    }

    private static void send(Socket socket, String msg) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(msg);
    }
}
