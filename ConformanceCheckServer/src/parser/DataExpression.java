package parser;

public abstract class DataExpression {
    protected Token node;

    public Token getNode() {
        return node;
    }
}
