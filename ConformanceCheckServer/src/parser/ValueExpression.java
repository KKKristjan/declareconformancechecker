package parser;

public class ValueExpression extends DataExpression{
    public ValueExpression(Token value) {
        this.node = value;
    }
    @Override
    public String toString() {
        return node.getValue();
    }
}
