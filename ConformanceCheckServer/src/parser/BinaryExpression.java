package parser;


public class BinaryExpression extends DataExpression{
    DataExpression left;
    DataExpression right;

    public BinaryExpression(Token op,DataExpression left, DataExpression right) {
        this.left = left;
        this.right = right;
        this.node = op;
    }

    public DataExpression getLeft() {
        return left;
    }

    public DataExpression getRight() {
        return right;
    }

    @Override
    public String toString() {
        return "BinaryExpression{" +
                "left=" + left +
                ", right=" + right +
                ", node=" + node +
                '}';
    }
}