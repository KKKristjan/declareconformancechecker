package parser;

import java.util.List;

public class DataFunction {
    List<String> args;
    DataExpression expression;

    public DataFunction(List<String> args, DataExpression expression) {
        this.args = args;
        this.expression = expression;
    }

    public List<String> getArgs() {
        return args;
    }

    public DataExpression getExpression(){
        return expression;
    }
}
