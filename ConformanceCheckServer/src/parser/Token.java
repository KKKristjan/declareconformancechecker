package parser;

public class Token {
    public enum Type {Activity, Variable, Operator, Set, Number, Comparator, Group, R}

    int position;
    Type type;
    String value;

    public Token(int position, Type type, String value) {
        this.position = position;
        this.type = type;
        this.value = value;
    }

    public int getPosition() {
        return position;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}