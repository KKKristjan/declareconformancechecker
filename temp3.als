abstract sig Activity {}
abstract sig Payload {}

abstract sig Event{
	task: one Activity,
	data: set Payload,
	tokens: set Token
}

one sig DummyPayload extends Payload {}
fact { no te:Event | DummyPayload in te.data }

one sig DummyActivity extends Activity {}

abstract sig Token {}
abstract sig SameToken extends Token {}
abstract sig DiffToken extends Token {}
lone sig DummySToken extends SameToken{}
lone sig DummyDToken extends DiffToken{}
fact { 
	no DummySToken
	no DummyDToken
	all te:Event| no (te.tokens & SameToken) or no (te.tokens & DiffToken)
}

pred True[]{some TE0}

// lang templates

pred Init(taskA: Activity) { 
	taskA = TE0.task
}

pred Existence(taskA: Activity) { 
	some te: Event | te.task = taskA
}

pred Existence(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } >= n
}

pred Absence(taskA: Activity) { 
	no te: Event | te.task = taskA
}

pred Absence(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } <= n
}

pred Exactly(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } = n
}

pred Choice(taskA, taskB: Activity) { 
	some te: Event | te.task = taskA or te.task = taskB
}

pred ExclusiveChoice(taskA, taskB: Activity) { 
	some te: Event | te.task = taskA or te.task = taskB
	(no te: Event | taskA = te.task) or (no te: Event | taskB = te.task )
}

pred RespondedExistence(taskA, taskB: Activity) {
	(some te: Event | taskA = te.task) implies (some ote: Event | taskB = ote.task)
}

pred Response(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[te, fte])
}

pred AlternateResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[te, fte] and (no ite: Event | taskA = ite.task and After[te, ite] and After[ite, fte]))
}

pred ChainResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and Next[te, fte])
}

pred Precedence(taskA, taskB: Activity) {
	all te: Event | taskB = te.task implies (some fte: Event | taskA = fte.task and After[fte, te])
}

pred AlternatePrecedence(taskA, taskB: Activity) {
	all te: Event | taskB = te.task implies (some fte: Event | taskA = fte.task and After[fte, te] and (no ite: Event | taskB = ite.task and After[fte, ite] and After[ite, te]))
}

pred ChainPrecedence(taskA, taskB: Activity) {
	all te: Event | taskB = te.task implies (some fte: Event | taskA = fte.task and Next[fte, te])
}

pred NotRespondedExistence(taskA, taskB: Activity) {
	(some te: Event | taskA = te.task) implies (no te: Event | taskB = te.task)
}

pred NotResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (no fte: Event | taskB = fte.task and After[te, fte])
}

pred NotPrecedence(taskA, taskB: Activity) {
	all te: Event | taskB = te.task implies (no fte: Event | taskA = fte.task and After[fte, te])
}

pred NotChainResponse(taskA, taskB: Activity) { 
	all te: Event | taskA = te.task implies (no fte: Event | (DummyActivity = fte.task or taskB = fte.task) and Next[te, fte])
}

pred NotChainPrecedence(taskA, taskB: Activity) {
	all te: Event | taskB = te.task implies (no fte: Event | (DummyActivity = fte.task or taskA = fte.task) and Next[fte, te])
}
//-

pred example { }
run example

---------------------- end of static code block ----------------------

--------------------- generated code starts here ---------------------

one sig complete  extends Activity {}
one sig A extends Activity {}
one sig B extends Activity {}
one sig C extends Activity {}
one sig D extends Activity {}
one sig E extends Activity {}
one sig F extends Activity {}
one sig TE0 extends Event {}{not task=DummyActivity}
one sig TE1 extends Event {}{not task=DummyActivity}
one sig TE2 extends Event {}{not task=DummyActivity}
one sig TE3 extends Event {}{not task=DummyActivity}
one sig TE4 extends Event {}{not task=DummyActivity}
one sig TE5 extends Event {}{not task=DummyActivity}
one sig TE6 extends Event {}{not task=DummyActivity}
pred Next(pre, next: Event){pre=TE0 and next=TE1 or pre=TE1 and next=TE2 or pre=TE2 and next=TE3 or pre=TE3 and next=TE4 or pre=TE4 and next=TE5 or pre=TE5 and next=TE6}
pred After(b, a: Event){// b=before, a=after
b=TE0 and not (a=TE0) or b=TE1 and not (a=TE1 or a=TE0) or b=TE2 and not (a=TE2 or a=TE0 or a=TE1) or b=TE3 and (a=TE6 or a=TE5 or a=TE4) or b=TE4 and (a=TE6 or a=TE5) or b=TE5 and (a=TE6)}
fact { all te: Event | te.task = A implies (one x & te.data)}
fact { all te: Event | te.task = B implies (one x & te.data)}
fact { all te: Event | te.task = C implies (one x & te.data)}
fact { all te: Event | te.task = D implies (one x & te.data)}
fact { all te: Event | te.task = E implies (one x & te.data)}
fact { all te: Event | te.task = F implies (one x & te.data)}
fact { all te: Event | lone(x & te.data) }
fact { all te: Event | some (x & te.data) implies te.task in (A + B + C + D + E + F) }
abstract sig x extends Payload {
amount: Int
}
fact { all te: Event | (lone x & te.data) }
pred Single(pl: x) {{pl.amount=1}}
fun Amount(pl: x): one Int {{pl.amount}}
one sig intEqualsTo0r100171 extends x{}{amount=1}
one sig intEqualsTo1r100172 extends x{}{amount=1}
one sig intBetween1and101r100173 extends x{}{amount=15}
pred p100174(A: Event) { { A.data&x in (intEqualsTo1r100172) } }
pred p100174c(A, T: set Event) { { T.data&x in (intEqualsTo1r100172) } }
fact {
all te: Event | (A = te.task and p100174[te]) implies (no ote: Event | C = ote.task and p100174c[te, ote])
}
fact {
E = TE0.task
intEqualsTo1r100172 = TE0.data & x
A = TE1.task
intEqualsTo1r100172 = TE1.data & x
B = TE2.task
intEqualsTo1r100172 = TE2.data & x
C = TE3.task
intEqualsTo1r100172 = TE3.data & x
D = TE4.task
intEqualsTo1r100172 = TE4.data & x
complete = TE5.task
complete = TE6.task

}
