
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeContinuousImpl;
import org.deckfour.xes.model.impl.XAttributeDiscreteImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.processmining.operationalsupport.xml.OSXMLConverter;

import java.io.*;
import java.net.*;
import java.util.List;


public class Evaluator {


    public static void main(String[] args) throws Exception {
        //Getting log and model
        String modelPath = args[0];
        String logPath = args[1];
        String model = readAllText(modelPath);
        XLog log = readTracesFromLogFile(logPath);
        //Getting the localhost stuff :https://www.journaldev.com/741/java-socket-programming-server-client
        InetAddress host = InetAddress.getLocalHost();
        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        System.out.println("Siin1");
        sendStuff(model, oos, ois, socket, host, "model");
        System.out.println("Siin3");
         // the new trace is gonna be something like this: <org.deckfour.xes.model.impl.XTraceImpl>   <log openxes.version="1.0RC7" xes.features="nested-attributes" xes.version="1.0" xmlns="http://www.xes-standard.org/"> 	<trace> 		<string key="concept:name" value="Case No. 1"/> 		<event> 			<string key="concept:name" value="C"/> 			<string key="lifecycle:transition" value="complete"/> 			<date key="time:timestamp" value="2019-02-14T04:30:31.015+02:00"/> 		</event> 	</trace> </log> </org.deckfour.xes.model.impl.XTraceImpl>

        Thread.sleep(200);
        for (XTrace t : log) {
            int k = 0;
            int traceSize = t.size();
            for (XEvent e : t) {

                System.out.println(k);
                String traceString = instanceChecker(e).toString();
                sendStuff(traceString, oos, ois, socket, host, "trace");
                //ois = new ObjectInputStream(socket.getInputStream());
                //String message = (String) ois.readObject();
                //System.out.println(message);
                k++;
                if (k == traceSize) {
                    System.out.println("Sending request to Socket Server");

                    socket = new Socket(host.getHostName(), 9876);
                    oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject("trace end");
                }
            }
        }
        socket = new Socket(host.getHostName(), 9876);
        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject("exit");
        oos.close();
    }

        /*
        System.out.println(log.get(0));
        for(XTrace t : log){
            for(XEvent e : t){
                System.out.println(instanceChecker(e));
                }
            }
        }*/

        /*
        String text = readAllText(modelPath);
        InetAddress host = InetAddress.getLocalHost();
        Socket socket;
        ObjectOutputStream oos;
        ObjectInputStream ois;
        socket = new Socket(host.getHostName(), 9876);
        oos = new ObjectOutputStream(socket.getOutputStream());
        System.out.println("Sending request to Socket Server");
        oos.writeObject(text);
        ois = new ObjectInputStream(socket.getInputStream());
        String message = (String) ois.readObject();
        System.out.println("Server: " + message);
        oos.writeObject("exit");
        ois.close();
        oos.close();
        Thread.sleep(100);


        if (message.equals("Received")){
            socket = new Socket(host.getHostName(), 9876);
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("exit");
        }
        else{
            return;
        }*/

    public static String readAllText(String filename) {
        StringBuilder sb = new StringBuilder(2048);
        try {
            FileInputStream is = new FileInputStream(filename);
            Reader r = new InputStreamReader(is, "UTF-8");
            int c;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    private static XLog readTracesFromLogFile(String filename) throws Exception {
        System.out.println("reading from:" + filename);
        FileInputStream fileIS = new FileInputStream(filename);
        List<XLog> log = new XesXmlParser().parse(fileIS);
        fileIS.close();
        return log.get(0);
    }


    public static StringBuilder instanceChecker(XEvent e){
        StringBuilder sb = new StringBuilder();
        for (XAttribute at : e.getAttributes().values()){
            if (at instanceof XAttributeLiteralImpl)
                sb.append("<string key=\"").append(at.getKey()).append("\" value=\"").append(((XAttributeLiteralImpl) at).getValue()).append("\"/> \n");
            if (at instanceof XAttributeDiscreteImpl){
                sb.append("<int key=\"" + at.getKey() + "\" value=\"" + ((XAttributeDiscreteImpl) at).getValue()+ "\"/> \n");
            }
            if (at instanceof XAttributeContinuousImpl){
                sb.append("<float key=\"" + at.getKey() + "\" value=\"" + ((XAttributeContinuousImpl) at).getValue()+ "\"/> \n");
            }
            if (at instanceof XAttributeTimestampImpl){
                sb.append("<date key=\"" + at.getKey() + "\" value=\"" + ((XAttributeTimestampImpl) at).toString()+ "\"/> \n");
            }
        }
        return sb;
    }

    public static void sendStuff(String thing,ObjectOutputStream oos, ObjectInputStream ois, Socket socket, InetAddress host, String type) throws IOException, ClassNotFoundException, InterruptedException {
        socket = new Socket(host.getHostName(), 9876);
        oos = new ObjectOutputStream(socket.getOutputStream());
        System.out.println("Sending request to Socket Server");
        oos.writeObject(type);
        oos = new ObjectOutputStream(socket.getOutputStream());
        Thread.sleep(100);
        oos.writeObject(thing);
        ois = new ObjectInputStream(socket.getInputStream());
        String message = (String) ois.readObject();
        System.out.println("Server: " + message);

        ois.close();
        oos.close();

    }
}


