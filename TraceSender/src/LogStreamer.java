
import info.clearthought.layout.TableLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XMxmlGZIPParser;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.*;
import org.deckfour.xes.model.impl.*;
import org.jdom.JDOMException;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import com.fluxicon.slickerbox.colors.SlickerColors;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;

public class LogStreamer extends JFrame{
    private static int PORT = 4444;
    private static String HOST;

    static {
        try {
            HOST = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
    }
    private static final long serialVersionUID = 1561407447457027863L;
    private JPanel contentPane;
    private Color backgroundColor = SlickerColors.COLOR_BG_4;
    private JRadioButton twentyfour;
    private JRadioButton sixty;
    private JRadioButton twelve;
    private JRadioButton six;
    private JRadioButton three;
    private JCheckBox check;
    private Action startAction;
    private JScrollPane instancesContainer;
    protected XLog log = null;
    protected JList instancesList;
    private String model;
    private OSXMLConverter osxmlConverter = new OSXMLConverter();
    List<String> activityNames = new ArrayList<>();


    @SuppressWarnings("serial")
    private void initActions() {
        startAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnabled(false);
            }

        };
        startAction.setEnabled(true);

    }

    private JScrollPane createSlickerScrollPane() {

        JScrollPane scrollpane = new JScrollPane();
        scrollpane.setOpaque(false);
        scrollpane.getViewport().setOpaque(false);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());
        //scrollpane.setViewportBorder(BorderFactory.createLineBorder(new Color(
        //		10, 10, 10), 2));
        scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getVerticalScrollBar().setOpaque(false);

        SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getHorizontalScrollBar().setOpaque(false);
        return scrollpane;
    }

    public LogStreamer(String confPath) throws JDOMException, IOException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        initActions();
        instancesList = new JList();
        instancesContainer = createSlickerScrollPane();
        instancesContainer.setSize(200, 800);
        instancesContainer.setPreferredSize(new Dimension(200,800));
        instancesContainer.setMaximumSize(new Dimension(200,800));
        instancesContainer.setMinimumSize(new Dimension(200,800));

        final SlickerFactory factory = SlickerFactory.instance();
        JPanel buttonPanel = factory.createRoundedPanel();
        JButton loadMap = factory.createButton("LOAD MODEL");
        loadMap.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                JFileChooser jfc = new JFileChooser();
                jfc.showOpenDialog(contentPane);
                if(jfc.getSelectedFile()!=null){// TODO your badboi routine
                    String inptModelFileName = jfc.getSelectedFile().getAbsolutePath();
                    if (inptModelFileName.toLowerCase().contains(".decl")) {//PORT 4444
                        model = readAllText(inptModelFileName);
                        JTextArea text = new JTextArea();
                        text.append(model);
                        text.setBounds(0,0,200,50);
                        Color grey = new Color(192,192,192);
                        text.setBackground(grey);
                        System.out.println(model);
                        SlickerFactory sf = SlickerFactory.instance();
                        JPanel panel = sf.createRoundedPanel();
                        panel.add(text);
                        instancesContainer.setViewportView(panel);
                    }
                }else{
                    ((JButton)e.getSource()).setBackground(factory.createButton("").getBackground());
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                // TODO Auto-generated method stub

            }
        });

        JButton loadLog = factory.createButton("LOAD LOG");
        loadLog.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent ev) {
                JFileChooser jfc = new JFileChooser();
                jfc.showOpenDialog(contentPane);
                if(jfc.getSelectedFile()!=null){
                    String inputLogFileName = jfc.getSelectedFile().getAbsolutePath();
					if(inputLogFileName.toLowerCase().contains("mxml.gz")){
						XMxmlGZIPParser parser = new XMxmlGZIPParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else if(inputLogFileName.toLowerCase().contains("mxml")){
						XMxmlParser parser = new XMxmlParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					if(inputLogFileName.toLowerCase().contains("xes.gz")){
						XesXmlGZIPParser parser = new XesXmlGZIPParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else if(inputLogFileName.toLowerCase().contains("xes")){
						XesXmlParser parser = new XesXmlParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
								int i = 0;
                                for(XTrace t : log){
								    //System.out.println(i++);
                                    /*String name1 = t.getAttributes().get("concept:name").toString();
                                    if (t.size() <= 50 && t.size() >= 45) {
                                        System.out.println("name1: " + name1);
                                        System.out.println("Size: " + t.size());
                                        i++;
                                        System.out.println("i: " + i++);
                                    }*/
								    for(XEvent e: t){
								        String name = e.getAttributes().get("concept:name").toString();
								        if (!activityNames.contains(name)){
								            activityNames.add(name);
								            System.out.println(name);
                                        }
                                    }
                                }
                                System.out.println("list size " + activityNames.size());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
                }else{
                    ((JButton)ev.getSource()).setBackground(factory.createButton("").getBackground());
                }
                if(log == null){
                    return;
                }



            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                // TODO Auto-generated method stub

            }
        });
        JButton startReplay = factory.createButton("START REPLAY");
        startReplay.addMouseListener(new MouseListener() {



            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
				if(log==null){
					JOptionPane.showMessageDialog(contentPane, "LOG MISSING!");
					return;
				}
				try{
					long times=0;
					if(sixty.isSelected()){
						times = 1000;
					}else if(twentyfour.isSelected()){
						times = 2500;
					}else if(twelve.isSelected()){
						times = 5000;
					}else if(six.isSelected()){
						times = 10000;
					}else{
						times = 20000;
					}
                    Socket socket = new Socket(HOST, PORT);
                    socket.isConnected();
                    PrintWriter writeOnTheSocket = new PrintWriter(socket.getOutputStream(), true);
                    writeOnTheSocket.println(model);
                    writeOnTheSocket.println("</model>");
                    writeOnTheSocket.flush();

                    // Start processing log
                    for (XTrace t : log) {
                         // if we need to add a random time before the trace is executed, this is the moment
                        long traceTimeIncrement = 0;

                        // some general trace statistics
                        int eventIndex = 0;


                        for (XEvent ev : t) {
                            XTraceImpl t1 = new XTraceImpl(t.getAttributes());
                            XAttributeTimestampImpl timestamp = (XAttributeTimestampImpl) ev.getAttributes().get("time:timestamp");
                            ((XAttributeTimestampImpl) ev.getAttributes().get("time:timestamp")).setValueMillis(timestamp.getValueMillis() + traceTimeIncrement + eventIndex);
                            // add trace attributes to event
                            String eventName = XConceptExtension.instance().extractName(ev);
                            ev.getAttributes().putAll(t.getAttributes());
                            XConceptExtension.instance().assignName(ev,eventName);
                            // Add event to trace
                            t1.add(ev);

                            System.out.println("Writing to socket Event : " + ev.getAttributes().get("concept:name"));
                            String packet = osxmlConverter.toXML(t1).replace('\n', ' ');
                            // Transmit trace

                            System.out.println(packet);
                            writeOnTheSocket.println(packet);
                            writeOnTheSocket.flush();
                            eventIndex++;
                            Thread.sleep(times);
                        }

                        XTraceImpl lastTrace = new XTraceImpl(t.getAttributes());
                        // Add last trace.
                        XFactory nxFactory = XFactoryRegistry.instance().currentDefault();
                        XEvent le = nxFactory.createEvent();
                        //le.setAttributes(t.get(t.size() - 1).getAttributes());
                        XConceptExtension nconcept = XConceptExtension.instance();
                        nconcept.assignName(le, "complete");
                        XLifecycleExtension nlc = XLifecycleExtension.instance();
                        nlc.assignTransition(le, "complete");
                        XTimeExtension ntimeExtension = XTimeExtension.instance();
                        ntimeExtension.assignTimestamp(le, ntimeExtension.extractTimestamp(t.get(t.size() - 1)).getTime() + 1);
                        lastTrace.add(le);
                        System.out.println("Writing to socket Event : " + le.getAttributes().get("concept:name"));
                        String packet = osxmlConverter.toXML(lastTrace).replace('\n', ' ');
                        // Transmit trace
                        writeOnTheSocket.println(packet);
                        System.out.println(packet);
                        writeOnTheSocket.flush();
                    }
                    socket.shutdownOutput();
					return;
				}catch (Exception ex){ex.printStackTrace();}
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                // TODO Auto-generated method stub

            }
        });
        JPanel optionPanel = factory.createRoundedPanel();
        JPanel radioPanel = factory.createRoundedPanel();
        radioPanel.setBorder(new TitledBorder("Speed"));
        optionPanel.setLayout(new BorderLayout());
        check = factory.createCheckBox("Complete traces in the log", true);
        sixty = factory.createRadioButton("60 ev. per min.");
        twentyfour = factory.createRadioButton("24 ev. per min.");
        twelve = factory.createRadioButton("12 ev. per min.");
        six = factory.createRadioButton("6 ev. per min.");
        three = factory.createRadioButton("3 ev. per min.");
        ButtonGroup bg = new ButtonGroup();
        bg.add(sixty);
        bg.add(twentyfour);
        bg.add(twelve);
        bg.add(six);
        bg.add(three);
        twelve.setSelected(true);
        radioPanel.setLayout(new TableLayout(new double[][] { { 300 }, {20, 20, 20, 20, 20 } }));
        radioPanel.add(sixty, "0,0");
        radioPanel.add(twentyfour, "0,1");
        radioPanel.add(twelve, "0,2");
        radioPanel.add(six, "0,3");
        radioPanel.add(three, "0,4");
        optionPanel.add(check, BorderLayout.EAST);
        optionPanel.add(radioPanel, BorderLayout.WEST);
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(optionPanel, BorderLayout.NORTH);
        buttonPanel.add(loadLog, BorderLayout.WEST);
        buttonPanel.add(loadMap, BorderLayout.CENTER);
        buttonPanel.add(startReplay, BorderLayout.EAST);
        contentPane = factory.createRoundedPanel();


        contentPane.setLayout(new BorderLayout());
        contentPane.add(buttonPanel, BorderLayout.SOUTH);
        contentPane.add(instancesContainer, BorderLayout.CENTER);
        contentPane.setSize(200, 800);
        contentPane.setPreferredSize(new Dimension(200,800));
        contentPane.setMaximumSize(new Dimension(200,800));
        contentPane.setMinimumSize(new Dimension(200,800));
        contentPane.setBackground(backgroundColor);
        setContentPane(contentPane);
        setSize(600, 700);
        setPreferredSize(new Dimension(600,700));
        setMaximumSize(new Dimension(600,700));
        setMinimumSize(new Dimension(600,700));
        setName("Log Replayer");
        setResizable(false);
        setTitle("Log Replayer");

    }

    public static String readAllText(String filename) {
        StringBuilder sb = new StringBuilder(2048);
        try {
            FileInputStream is = new FileInputStream(filename);
            Reader r = new InputStreamReader(is, "UTF-8");
            int c;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }


    public static void main(String[] args) {
        try {
            if(args.length>0){
                new LogStreamer(args[0]).setVisible(true);
            }else{
                new LogStreamer("conf.xml").setVisible(true);
            }
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}


