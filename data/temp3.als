abstract sig Activity {}
abstract sig Payload {}

abstract sig Event{
	task: one Activity,
	data: set Payload,
	tokens: set Token
}

one sig DummyPayload extends Payload {}
fact { no te:Event | DummyPayload in te.data }

one sig DummyActivity extends Activity {}

abstract sig Token {}
abstract sig SameToken extends Token {}
abstract sig DiffToken extends Token {}
lone sig DummySToken extends SameToken{}
lone sig DummyDToken extends DiffToken{}
fact { 
	no DummySToken
	no DummyDToken
	all te:Event| no (te.tokens & SameToken) or no (te.tokens & DiffToken)
}

pred True[]{some TE0}

// lang templates

pred Init(taskA: Activity) { 
	taskA = TE0.task
}

pred Existence(taskA: Activity) { 
	some te: Event | te.task = taskA
}

pred Existence(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } >= n
}

pred Absence(taskA: Activity) { 
	no te: Event | te.task = taskA
}

pred Absence(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } <= n
}

pred Exactly(taskA: Activity, n: Int) {
	#{ te: Event | taskA = te.task } = n
}

pred Choice(taskA, taskB: Activity) { 
	some te: Event | te.task = taskA or te.task = taskB
}

pred ExclusiveChoice(taskA, taskB: Activity) { 
	some te: Event | te.task = taskA or te.task = taskB
	(no te: Event | taskA = te.task) or (no te: Event | taskB = te.task )
}

pred RespondedExistence(taskA, taskB: Activity) {
	(some te: Event | taskA = te.task) implies (some ote: Event | taskB = ote.task)
}

pred Response(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[te, fte])
}

pred AlternateResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[te, fte] and (no ite: Event | taskA = ite.task and After[te, ite] and After[ite, fte]))
}

pred ChainResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and Next[te, fte])
}

pred Precedence(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[fte, te])
}

pred AlternatePrecedence(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and After[fte, te] and (no ite: Event | taskA = ite.task and After[fte, ite] and After[ite, te]))
}

pred ChainPrecedence(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (some fte: Event | taskB = fte.task and Next[fte, te])
}

pred NotRespondedExistence(taskA, taskB: Activity) {
	(some te: Event | taskA = te.task) implies (no te: Event | taskB = te.task)
}

pred NotResponse(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (no fte: Event | taskB = fte.task and After[te, fte])
}

pred NotPrecedence(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (no fte: Event | taskB = fte.task and After[fte, te])
}

pred NotChainResponse(taskA, taskB: Activity) { 
	all te: Event | taskA = te.task implies (no fte: Event | (DummyActivity = fte.task or taskB = fte.task) and Next[te, fte])
}

pred NotChainPrecedence(taskA, taskB: Activity) {
	all te: Event | taskA = te.task implies (no fte: Event | (DummyActivity = fte.task or taskB = fte.task) and Next[fte, te])
}
//-

pred example { }
run example

---------------------- end of static code block ----------------------

--------------------- generated code starts here ---------------------

one sig complete  extends Activity {}
one sig ApplyForTrip extends Activity {}
one sig BookTransport extends Activity {}
one sig BookAccommodation extends Activity {}
one sig CollectTickets extends Activity {}
one sig TE0 extends Event {}{not task=DummyActivity}
one sig TE1 extends Event {}{not task=DummyActivity}
one sig TE2 extends Event {}{not task=DummyActivity}
one sig TE3 extends Event {}{not task=DummyActivity}
one sig TE4 extends Event {}{not task=DummyActivity}
one sig TE5 extends Event {}{not task=DummyActivity}
one sig TE6 extends Event {}
one sig TE7 extends Event {}
one sig TE8 extends Event {}
one sig TE9 extends Event {}
one sig TE10 extends Event {}
one sig TE11 extends Event {}
one sig TE12 extends Event {}
one sig TE13 extends Event {}
one sig TE14 extends Event {}
one sig TE15 extends Event {}
one sig TE16 extends Event {}
pred Next(pre, next: Event){pre=TE0 and next=TE1 or pre=TE1 and next=TE2 or pre=TE2 and next=TE3 or pre=TE3 and next=TE4 or pre=TE4 and next=TE5 or pre=TE5 and next=TE6 or pre=TE6 and next=TE7 or pre=TE7 and next=TE8 or pre=TE8 and next=TE9 or pre=TE9 and next=TE10 or pre=TE10 and next=TE11 or pre=TE11 and next=TE12 or pre=TE12 and next=TE13 or pre=TE13 and next=TE14 or pre=TE14 and next=TE15 or pre=TE15 and next=TE16}
pred After(b, a: Event){// b=before, a=after
(b=TE0 and not a = TE0)or a=TE16 or b=TE1 and not (a=TE0 or a = TE1) or b=TE2 and not (a=TE0 or a=TE1 or a = TE2 ) or b=TE3 and not (a=TE0 or a=TE1 or a=TE2) or
b=TE4 and not (a=TE0 or a=TE1 or a=TE2 or a=TE3) or b=TE5 and not (a=TE0 or a=TE1 or a=TE2 or a=TE3 or a=TE4) or b=TE6
and not (a=TE0 or a=TE1 or a=TE2 or a=TE3 or a=TE4 or a=TE5) or b=TE7 and not (a=TE0 or a=TE1 or a=TE2 or a=TE3 or a=TE4 or a=TE5 or a=TE6)
or b=TE8 and (a=TE16 or a=TE15 or a=TE14 or a=TE13 or a=TE12 or a=TE11 or a=TE10 or a=TE9) or b=TE9 and (a=TE16 or a=TE15 or a=TE14 or a=TE13
or a=TE12 or a=TE11 or a=TE10) or b=TE10 and (a=TE16 or a=TE15 or a=TE14 or a=TE13 or a=TE12 or a=TE11) or b=TE11
and (a=TE16 or a=TE15 or a=TE14 or a=TE13 or a=TE12) or b=TE12 and (a=TE16 or a=TE15 or a=TE14 or a=TE13) or b=TE13 and (a=TE16 or a=TE15 or a=TE14) or b=TE14 and (a=TE16 or a=TE15 or a = TE14)}
fact { all te: Event | te.task = CollectTickets implies (one Price & te.data)}
fact { all te: Event | te.task = BookTransport implies (one TransportType & te.data and one Price & te.data)}
fact { all te: Event | lone(Price & te.data) }
fact { all te: Event | some (Price & te.data) implies te.task in (CollectTickets + BookTransport) }
fact { all te: Event | lone(TransportType & te.data) }
fact { all te: Event | some (TransportType & te.data) implies te.task in (BookTransport) }
abstract sig TransportType extends Payload {}
fact { all te: Event | (lone TransportType & te.data)}
one sig Car extends TransportType{}
one sig Plane extends TransportType{}
one sig Train extends TransportType{}
one sig Bus extends TransportType{}
abstract sig Price extends Payload {
amount: Int
}
fact { all te: Event | (lone Price & te.data) }
pred Single(pl: Price) {{pl.amount=1}}
fun Amount(pl: Price): one Int {{pl.amount}}
one sig floatBetween30p0and30p0r101475 extends Price{}{amount=15}
one sig floatEqualsTo30p0r101473 extends Price{}{amount=1}
one sig floatBetween0p0and30p0r101474 extends Price{}{amount=15}
one sig floatBetween30p0and100p0r101476 extends Price{}{amount=15}
pred p101477(A: Event) { { A.data&Price in (floatBetween0p0and30p0r101474) } }
pred p101477c(A, B: Event) { { B.data&Price in (floatBetween30p0and100p0r101476) } }
fact {
Precedence[CollectTickets,BookTransport]
Existence[CollectTickets]
all te: Event | (BookTransport = te.task and p101477[te]) implies (some fte: Event | BookTransport = fte.task and p101477c[te, fte] and After[te, fte])
}
fact {
ApplyForTrip = TE0.task
BookTransport = TE1.task
Car = TE1.data & TransportType
floatBetween30p0and100p0r101476 = TE1.data & Price
BookTransport = TE2.task
Car = TE2.data & TransportType
floatBetween0p0and30p0r101474 = TE2.data & Price
BookTransport = TE3.task
Bus = TE3.data & TransportType
floatBetween0p0and30p0r101474 = TE3.data & Price
BookTransport = TE4.task
Bus = TE4.data & TransportType
floatBetween0p0and30p0r101474 = TE4.data & Price
BookTransport = TE5.task
Bus = TE5.data & TransportType
floatBetween0p0and30p0r101474 = TE5.data & Price

}
